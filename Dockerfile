FROM php:7.1-fpm

RUN echo 'DPkg::Post-Invoke {"/bin/rm -f /var/cache/apt/archives/*.deb || true";};' | tee /etc/apt/apt.conf.d/clean

RUN apt-get update && apt-get install -y \
        libicu-dev \
	zlib1g-dev \
	libsqlite3-dev \
	libpq-dev \
	libicu52 \
	sqlite3 \
	git \
	libc6 \
	libfreetype6 \
	libgd3 \
	libjpeg62-turbo \
	libpng12-0 \
	libvpx1 \
	libx11-6 \
	libxpm4 \
	ucf \
	zlib1g \
	libpng-dev \
    && docker-php-ext-install \
        zip \
	intl \
	mbstring \
	pdo \
	pdo_mysql \
	pdo_pgsql \
	pgsql \
	pdo_sqlite \
	gd \
    && docker-php-ext-enable \
        zip \
	intl \
	mbstring \
	pdo \
	pdo_mysql \
	pdo_pgsql \
	pgsql \
	pdo_sqlite \
	gd 
